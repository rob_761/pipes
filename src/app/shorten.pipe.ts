import {PipeTransform, Pipe} from "@angular/core";
@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform{

  transform(value: any, starLimit: number, endLimit: number) {
    if (value.length > Math.abs(endLimit - starLimit)) {
      return value.substr(starLimit, endLimit) + ' ...';
    }
    return value;
  }
}
